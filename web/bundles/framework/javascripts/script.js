//- time display alert
$(function() {
    $('.alert-success').delay( 3000 ).slideUp();
    $('.alert-error').delay( 3000 ).slideUp();
});

//- button update adminboard
$('#button-admin-update').click(function() {
    $('.form-admin-read').fadeOut('slow', function() {
        $('.form-admin-update').fadeIn('slow');
    });
    $('#button-admin-update').fadeOut('slow', function() {
        $('#button-admin-back').fadeIn('slow');
        $( '.button-create-admin' ).animate({ opacity: 0 }, 100);
        $( '.button-delete-admin' ).animate({ opacity: 0 }, 100);
    });
});

//- button back admin
$('#button-admin-back').click(function() {
    $('.form-admin-update').fadeOut('slow', function() {
        $('.form-admin-read').fadeIn('slow');
    });
    $('#button-admin-back').fadeOut('slow', function() {
        $('#button-admin-update').fadeIn('slow');
        $( '.button-create-admin' ).animate({ opacity: 1 }, 100);
        $( '.button-delete-admin' ).animate({ opacity: 1 }, 100);
    });
});

//- button create admin
$('#button-create-admin').click(function() {
    $( '.button-update-admin' ).animate({ opacity: 0 }, 500);
    $( '.button-delete-admin' ).animate({ opacity: 0 }, 500);
    $('.button-create-admin').fadeOut('slow', function() {
        $('.form-admin-read').fadeOut('slow', function() {
            $('.form-create-admin').fadeIn('slow', function() {
                    $('.button-create-back').fadeIn('slow');
                });
            });
    });
});

//- button back create
$('#button-create-back').click(function() {
    $( '.button-update-admin' ).animate({ opacity: 1 }, 1500);
    $( '.button-delete-admin' ).animate({ opacity: 1 }, 1500);
    $('.button-create-back').fadeOut('slow', function() {
        $('.form-create-admin').fadeOut('slow', function() {
            $('.form-admin-read').fadeIn('slow', function() {
                    $('.button-create-admin').fadeIn('slow');
                });
            });
    });
});

//- admin breadcrumb
$('.breadcrumb-profile').click(function() {
    $('.content-list-admin').fadeOut(function() {
        $('.content-profil-admin').fadeIn();
    });
    $('.breadcrumb-profile').css({
        "background-color": "#61C7B3",
        "color": "#FFFFFF"
    });
    $('.breadcrumb-listadmin').css({
        "background-color": "#FFFFFF",
        "color": "#61C7B3"
    });
});

$('.breadcrumb-listadmin').click(function() {
    $('.content-profil-admin').fadeOut(function() {
        $('.content-list-admin').fadeIn();
    });
    $('.breadcrumb-listadmin').css({
        "background-color": "#61C7B3",
        "color": "#FFFFFF"
    });
    $('.breadcrumb-profile').css({
        "background-color": "#FFFFFF",
        "color": "#61C7B3"
    });
});
