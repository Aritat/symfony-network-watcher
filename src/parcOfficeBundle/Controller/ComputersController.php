<?php

namespace parcOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use parcOfficeBundle\Entity\Computer;
use parcOfficeBundle\Entity\Log;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class ComputersController extends Controller
{
    public function indexAction(Request $request)
    {
        $session = $request->getSession();
        $id = $request->attributes->get('id');
        $em = $this->getDoctrine()->getManager();
        $computer = $em->getRepository('parcOfficeBundle:Computer')->find($id);
        $em = $this->getDoctrine()->getManager();
        $room = $em->getRepository('parcOfficeBundle:Room')->find($computer->getIdRoom());
        $query = $em->createQuery(
                    'SELECT c
                FROM parcOfficeBundle:Log c
                WHERE c.id_computer = :id
                ORDER BY c.id ASC'
                )->setParameter('id', $id);
		$ip=$computer->getIp();
                $allLog = $query->getResult();
 		$response = $this->get('templating');

        if ($session->get('connected') == true) {
            if ($request->isMethod('GET')) {
                $response = $this->get('templating')
                    ->render('parcOfficeBundle:Computers:index.html.twig', array('allLog'=>$allLog,'computer' => $computer, 'room' => $room, 'session' => $session->get('connected'), 'id_admin' => $session->get('id_admin')));
                return new Response($response);
            } else {
                $session->getFlashBag()->add('error', 'An error occured');
                return $this->redirectToRoute('parc_office_homepage');
            }
        }
        else {
            $response = $this->get('templating')
                ->render('parcOfficeBundle:Computers:index.html.twig', array('computer' => $computer, 'room' => $room));
            return new Response($response);
        }
    }

    public function formAction(Request $request)
    {
        $session = $request->getSession();

        //- we check if user is connected
        if ($session->get('connected') == true) {

            //- if user is connected and method == POST
            if ($request->isMethod("POST")) {

                $room = $request->request->get('room');

                return $this->render('parcOfficeBundle:Computers:add.html.twig', array('room' => $room, 'session' => $session->get('connected'), 'id_admin' => $session->get('id_admin')));

            }
            else {
                $session->getFlashBag()->add('error', 'You must be logged to update a computer');
                return $this->redirectToRoute('parc_office_homepage');
            }
        }
        else {

        }
    }

    public function addAction(Request $request)
    {
        $session = $request->getSession();

        if ($session->get('connected') == true) {

            //- if user is connected and method == POST
            if ($request->isMethod("POST")) {

                $name = $request->request->get('name');
                $ip = $request->request->get('ip');
                $idroom = $request->request->get('room');

                $em = $this->getDoctrine()->getManager();
                $allreadyinbdd = $em->getRepository('parcOfficeBundle:Computer')->findOneBy(array('ip' => $ip));


                if ($name == null || $ip == null || $idroom == null) {
                    $session->getFlashBag()->add('error', 'Error in your form');
                    return $this->redirectToRoute('parc_office_homepage', array('session' => $session->get('connected'), 'id_admin' => $session->get('id_admin')));
                }
                else {
                    if ($allreadyinbdd){
                        $session->getFlashBag()->add('error', 'A computer with this IP is already registered.');
                        return $this->redirectToRoute('parcoffice_room_select', array('id' => $idroom,'session' => $session->get('connected'), 'id_admin' => $session->get('id_admin')));

                    }
                    else {
                        $ct = new Computer();
                        $ct->setName($name);
                        $ct->setIp($ip);
                        $ct->setIdRoom($idroom);
                        $ct->setDateCreation(date_create());

                        $em = $this->getDoctrine()->getManager();

                        $em->persist($ct);

                        $em->flush();

                        $message = "ajout de la machine au parc";
                        $this->createLog($ip, $message);
			
			$filename = "/home/lucas/".$ip."/infosystem.txt";
			$handle = fopen($filename, 'r');
			$contents = fread($handle, filesize($filename));
			fclose($handle);
			$this->createLog($ip, $contents);

                        $session->getFlashBag()->add('success', 'New computer was registered!');

                        return $this->redirectToRoute('parcoffice_room_select', array('id' => $idroom,'session' => $session->get('connected'), 'id_admin' => $session->get('id_admin')));
                    }
                }

            }
            else {
                $session->getFlashBag()->add('warning', 'Something goes wrong, please retry later.');
                return $this->redirectToRoute('parcoffice_computer', array('session' => $session->get('connected'), 'id_admin' => $session->get('id_admin')));
            }
        }
        else {


            //- session flash alert error
            $session->getFlashBag()->add('error', 'You must be connected to access this page.');

            return $this->redirectToRoute('parcoffice_computer');

        }
    }

    public function editAction(Request $request)
    {

        $session = $request->getSession();

        if ($request->isMethod("POST")) {

            if ($session->get('connected') != true) {

                $session->getFlashBag()->add('error', 'You must be logged in as administrator to change');
                return $this->redirectToRoute('parc_office_homepage');

            } else {

                $id = $request->attributes->get('id');
                $name = trim($request->request->get('name'));
                $ip = trim($request->request->get('ip'));
                $room = trim($request->request->get('newroom'));

                $em = $this->getDoctrine()->getManager();
                $a = $em->getRepository('parcOfficeBundle:Computer')->find($id);

                if ($name == null || $ip == null || $room == null) {

                    $session->getFlashBag()->add('error', 'An error occured, please try again');
                    return $this->redirectToRoute('parcoffice_admin', array('id' => $a->getId()));

                } else {

                    $a->setName($name);
                    $a->setIp($ip);
                    $a->setIdRoom($room);

                    $em->flush();

                    $message = "modification d'attribut de la machine";
                    $this->createLog($ip, $message);
                    $session->getFlashBag()->add('success', 'Computer has been updated');

                    return $this->redirectToRoute('parcoffice_computer', array('id' => $a->getId()));

                }

            }

        } else {

            $id = $request->attributes->get('id');

            $em = $this->getDoctrine()->getEntityManager();
            $computer = $em->getRepository('parcOfficeBundle:Computer')->find($id);

            $computerRoom = $computer->getIdRoom();

            $rooms = $em->getRepository('parcOfficeBundle:Room')->findAll();

            $room = $em->getRepository('parcOfficeBundle:Room')->find($computerRoom);

            return $this->render('parcOfficeBundle:Computers:edit.html.twig', array('computer' => $computer, 'roomname' => $room, 'rooms' => $rooms, 'session' => $session->get('connected'), 'id_admin' => $session->get('id_admin')));

        }
    }

    public function deleteAction(Request $request){

        $session = $request->getSession();

        if ($session->get('connected') == true) {
            $session = $request->getSession();
            $id = $request->attributes->get('id');
            $em = $this->getDoctrine()->getEntityManager();
            $computerentities = $em->getRepository('parcOfficeBundle:Computer')->find($id);
            $idroom = $computerentities->getIdRoom();
            $em->remove($computerentities);
            $em->flush();
            $session->getFlashBag()->add('success','Computer has been deleted');
            return $this->redirectToRoute('parcoffice_room_select', array('id' => $idroom,'session' => $session->get('connected'), 'id_admin' => $session->get('id_admin')));
        }
        else {
            $session->getFlashBag()->add('error', 'You must be logged to delete a computer');
            return $this->redirectToRoute('parc_office_homepage');
        }

    }

    public function createLog($ip,$message)
    {
        //recherche de la machine concernée par le log
        $repository = $this->getDoctrine()->getRepository('parcOfficeBundle:Computer');
        $computer = $repository->findOneBy(
            array('ip' => $ip)
        );
        $id_computer = $computer->getId();
        $content_log =
            "ip :             "       . $ip
            . " , id_machine :  "       . $computer->getId()
            . " , nom_machine : "       . $computer->getName()
            . " , room :        "       . $computer->getIdRoom()
            . " , message :     "       . $message;
        $log = new Log();
        $log->setContentLog($content_log);
        $log->setDateCreation(new \DateTime());
        $log->setIdComputer($id_computer);
        $em = $this->getDoctrine()->getManager();
        $em->persist($log);
        $em->flush();
    }
}
