<?php

namespace parcOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use parcOfficeBundle\Entity\Room;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class RoomController extends Controller
{

    public function indexAction(Request $request)
    {

        $session = $request->getSession();

        if ($request->isMethod('GET')) {

            $repository = $this->getDoctrine()->getRepository('parcOfficeBundle:Room');
            $allRooms = $repository->findAll();

            if ($session->get('connected')) {
                $response = $this->get('templating')
                    ->render('parcOfficeBundle:Room:index.html.twig', array('allrooms' => $allRooms, 'session' => $session->get('connected'), 'id_admin' => $session->get('id_admin')));

                return new Response($response);
            } else {
                $response = $this->get('templating')
                    ->render('parcOfficeBundle:Room:index.html.twig', array('allrooms' => $allRooms));

                return new Response($response);
            }

        }

    }

    public function selectAction(Request $request)
    {

        $session = $request->getSession();

        $id = $request->attributes->get('id');

        //- query for 1 room
        $em = $this->getDoctrine()->getManager();
        $room = $em->getRepository('parcOfficeBundle:Room')->find($id);

        $query = $em->createQuery(
            'SELECT c
            FROM parcOfficeBundle:Computer c
            WHERE c.id_room = :id
            ORDER BY c.id ASC'
        )->setParameter('id', $id);

        $allComputers = $query->getResult();

        if ($request->isMethod('GET') && $session->get('connected')) {
            // var_dump($room->getName());
            $response = $this->get('templating')
                ->render('parcOfficeBundle:Room:room.html.twig', array('allComputers' => $allComputers, 'roomName' => $room->getName(), 'idName' => $room->getId(), 'session' => $session->get('connected'), 'id_admin' => $session->get('id_admin')));

            return new Response($response);

        }
        else {
          $response = $this->get('templating')
              ->render('parcOfficeBundle:Room:room.html.twig', array('allComputers' => $allComputers, 'roomName' => $room->getName(), 'idName' => $room->getId()));

          return new Response($response);
        }

    }

    public function createAction(Request $request)
    {

        $session = $request->getSession();

        if ($request->isMethod('POST')) {

            if ($session->get('connected') != true) {

                $session->getFlashBag()->add('error', 'You must be logged in as administrator to create a room');
                return $this->redirectToRoute('parc_office_homepage');

            } else {

                $name_room = trim($request->request->get('name'));

                if ($name_room == null) {

                    $session->getFlashBag()->add('error', 'Field room\'s name is empty !');
                    return $this->redirectToRoute('parc_office_homepage');
                } else {

                    $repository = $this->getDoctrine()->getRepository('parcOfficeBundle:Room');
                    $room = $repository->findOneBy(array('name' => $name_room));

                    if ($room) {
                        //- Le nom de la room existe deja
                        $session->getFlashBag()->add('error', 'Room\'s name is already use');
                        return $this->redirectToRoute('parc_office_homepage');

                    } else {

                        $room = new Room();
                        $room->setName($name_room);
                        $room->setDateCreation(new \DateTime());
                        $datacontext = $this->getDoctrine()->getManager();

                        $datacontext->persist($room);
                        $datacontext->flush();

                        $session->getFlashBag()->add('success', 'New room create !');
                        return $this->redirectToRoute('parc_office_homepage');

                    }
                }
            }

        } else {

            $session->getFlashBag()->add('error', 'An error occured, try again');
            return $this->redirectToRoute('parc_office_homepage');

        }

    }

    public function updateAction(Request $request)
    {
        $session = $request->getSession();

        if ($session->get('connected') != true) {

            $session->getFlashBag()->add('error', 'You must be logged in as administrator to change');
            return $this->redirectToRoute('parc_office_homepage');

        } else {

            if ($request->isMethod("POST")) {

                $id = $request->attributes->get('id');
                $name_room = trim($request->request->get('name'));

                //- query to check user to update
                $em = $this->getDoctrine()->getManager();
                $room = $em->getRepository('parcOfficeBundle:Room')->find($id);

                if ($name_room == null) {

                    $session->getFlashBag()->add('error', 'An error occured, field name is empty');
                    return $this->redirectToRoute('parc_office_homepage');

                } else {

                    $room->setName($name_room);

                    $em->flush();

                    $session->getFlashBag()->add('success', 'The room has been updated');

                    return $this->redirectToRoute('parc_office_homepage');

                }

            } else {

                $session->getFlashBag()->add('error', 'Error 404');
                return $this->redirectToRoute('parc_office_homepage');
            }
        }
    }

    public function deleteAction(Request $request)
    {

        $session = $request->getSession();

        if ($session->get('connected') != true) {

            $session->getFlashBag()->add('error', 'You must be logged in as administrator to change');
            return $this->redirectToRoute('parc_office_homepage');

        } else {
            $id = $request->attributes->get('id');
            $em = $this->getDoctrine()->getEntityManager();
            $query = $em->createQuery(
                'SELECT c
                FROM parcOfficeBundle:Computer c
                WHERE c.id_room = :id
                ORDER BY c.id ASC'
            )->setParameter('id', $id);
            $room = $query->getResult();
            if ($room)
            {
              $session->getFlashBag()->add('error', 'You can\'t delete a room with a computer on it');
              return $this->redirectToRoute('parcoffice_room_select', array('id' => $id));
            }
            else {
              $roomentities = $em->getRepository('parcOfficeBundle:Room')->find($id);
              $em->remove($roomentities);
              $em->flush();
            }
            return $this->redirectToRoute('parc_office_homepage');

        }
    }

    public function renameAction(Request $request){

        $session = $request->getSession();

        if ($request->isMethod('GET')) {

            if ($session->get('connected') != true) {

                $session->getFlashBag()->add('error', 'You must be logged in as administrator to rename a room');
                return $this->redirectToRoute('parc_office_homepage');

            } else {

                $idroom = $request->attributes->get('id');

                $em = $this->getDoctrine()->getManager();
                $room = $em->getRepository('parcOfficeBundle:Room')->find($idroom);

                $response = $this->get('templating')
                    ->render('parcOfficeBundle:Room:rename.html.twig', array('roomName' => $room->getName(), 'roomId' => $room->getId(),'session' => $session->get('connected'), 'id_admin' => $session->get('id_admin')));

                return new Response($response);
            }
        }
    }

}
