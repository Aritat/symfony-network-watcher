<?php

namespace parcOfficeBundle\Controller;

use parcOfficeBundle\Entity\Administrator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class AdminController extends Controller
{
    /**
     * @Route("/admin")
     */
    public function indexAction(Request $request)
    {
        $session = $request->getSession();

        //- if admin is connected, he can go in this page
        if ($session->get('connected') == true) {

            if ($request->isMethod('GET')) {
                $id = $request->attributes->get('id');
            }

            $repository = $this->getDoctrine()->getRepository('parcOfficeBundle:Administrator');

            $admin = $repository->findOneBy(
                array('id' => $id)
            );

            if ($admin) {

                //- query for all admins
                $repository = $this->getDoctrine()->getRepository('parcOfficeBundle:Administrator');
                $allAdmins = $repository->findAll();

                //- if admin exist redirection to adminboard
                $response = $this->get('templating')
                    ->render('parcOfficeBundle:Admin:index.html.twig', array('admin' => $admin, 'alladmins' => $allAdmins, 'session' => $session->get('connected'), 'id_admin' => $session->get('id_admin')));

                return new Response($response);

            } else {
                //- if admin doesnt exist
                $session->getFlashBag()->add('error', 'Log in to access this page');

                return $this->redirectToRoute('parc_office_homepage');
            }

        } else {
            //- if admin is not connected, redirection
            $session->getFlashBag()->add('error', 'Log in to access this page');

            return $this->redirectToRoute('parc_office_homepage');

        }
    }

    public function createAction(Request $request)
    {
        // $session = new Session();
        $session = $request->getSession();

        if ($request->isMethod("POST")) {

            if ($session->get('connected') != true) {

                $session->getFlashBag()->add('error', 'You must be logged in as administrator to create');
                return $this->redirectToRoute('parc_office_homepage');

            } else {

                $email = trim($request->request->get('email'));

                if ($email == null) {
                    //- Session flash alert - redirect
                    $session->getFlashBag()->add('error', 'Field e-mail is empty');

                    return $this->redirectToRoute('parc_office_homepage');

                } else {
                    //- query to check email in database
                    $repository = $this->getDoctrine()->getRepository('parcOfficeBundle:Administrator');

                    $admin = $repository->findOneBy(array('email' => $email));

                    if ($admin) {
                        //- if email is already use
                        $session->getFlashBag()->add('error', 'E-mail is already use');

                        return $this->redirectToRoute('parc_office_homepage');

                    } else {
                        //- if email is not already use
                        //- create pseudo uniq
                        $pseudo = 'admin'.time();
                        $password = crypt($email, "sjkqhjsqhskqjhsqlkjhsqlkh");

                        $admin = new Administrator();
                        $admin->setPseudo($pseudo);
                        $admin->setEmail($email);
                        $admin->setPassword($password);
                        $admin->setDateCreation(date_create());

                        $datacontext = $this->getDoctrine()->getManager();

                        // tells Doctrine you want to (eventually) save the Product (no queries yet)
                        $datacontext->persist($admin);

                        // actually executes the queries (i.e. the INSERT query)
                        $datacontext->flush();

                        //- session flash alert succes
                        $session->getFlashBag()->add('success', 'Registering done ! Please change your password for the security');

                        //- query to check id of user create
                        $repository = $this->getDoctrine()->getRepository('parcOfficeBundle:Administrator');

                        $a = $repository->findOneBy(array('email' => $email));

                        // var_dump($session->get('email'));

                        return $this->redirectToRoute('parcoffice_admin', array('id' => $a->getId()));

                    }
                }

            }

        } else {

            $session->getFlashBag()->add('error', 'Log in to access this page');
            return $this->redirectToRoute('parc_office_homepage');

        }
    }

    public function updateAction(Request $request)
    {
        $session = $request->getSession();

        if ($request->isMethod("POST")) {

            if ($session->get('connected') != true) {

                $session->getFlashBag()->add('error', 'You must be logged in as administrator to change');
                return $this->redirectToRoute('parc_office_homepage');

            } else {

                $id = $request->attributes->get('id');
                $pseudo = trim($request->request->get('pseudo'));
                $email = trim($request->request->get('email'));
                $password = trim($request->request->get('password'));

                //- transform pseudo in lowercase & escape spaces in string
                $pseudo = strtolower($pseudo);
                $pseudo = str_replace(CHR(32), "", $pseudo);

                //- query to check user to update
                $em = $this->getDoctrine()->getManager();
                $a = $em->getRepository('parcOfficeBundle:Administrator')->find($id);

                if ($pseudo == null || $email == null || $password == null) {

                    $session->getFlashBag()->add('error', 'An error occured, please try again');
                    return $this->redirectToRoute('parcoffice_admin', array('id' => $a->getId()));

                } else {

                    $passwordNew = crypt($password, "sjkqhjsqhskqjhsqlkjhsqlkh");

                    $a->setPseudo($pseudo);
                    $a->setEmail($email);
                    if ($password !== $a->getPassword()){
                        $a->setPassword($passwordNew);
                    }

                    $em->flush();

                    $session->getFlashBag()->add('success', 'Your profile has been updated');

                    return $this->redirectToRoute('parcoffice_admin', array('id' => $a->getId()));

                }

            }

        } else {

            $session->getFlashBag()->add('error', 'Log in to access this page');

            return $this->redirectToRoute('parc_office_homepage');

        }
    }

    //SIGN IN VERIFICATION
    public function signInAction(Request $request){

        if($request->isMethod('POST')){

            $session = $request->getSession();
            $pseudo = $request->request->get('pseudo');
            $password = $request->request->get('password');


            if($pseudo == null || $password == null){

                $session->getFlashBag()->add('error', 'Field pseudo or password is empty');

                return $this->redirectToRoute('parc_office_homepage');

            } else {

                $repository = $this->getDoctrine()->getRepository('parcOfficeBundle:Administrator');

                $admin = $repository->findOneBy(array('pseudo' => $pseudo));

                if(!$admin)
                {
                    $session->getFlashBag()->add('error', 'Pseudo doesn\'t exist');

                    return $this->redirectToRoute('parc_office_homepage');

                } else {

                    $hash = crypt($password,'sjkqhjsqhskqjhsqlkjhsqlkh');

                    if($hash != $admin->getPassword()){

                        $session->getFlashBag()->add('error', 'Password is incorrect');

                        return $this->redirectToRoute('parc_office_homepage');

                    } else {

                        $session->set('connected', true);
                        $session->set('id_admin', $admin->getId());

                        // var_dump($admin->getId());

                        $session->getFlashBag()->add('success', 'Sign in successfull !');

                        return $this->redirectToRoute('parc_office_homepage', array('id' => $admin->getId()));

                    }

                }

            }

        }
    }

    //LOGOUT PROCESS
    public function logoutAction(Request $request){

        $session = $request->getSession();
        $session->remove('connected');
        $session->remove('id_admin');
        $session->clear();
        $session->getFlashBag()->add('success','Log out successfull !');

        return $this->redirectToRoute('parc_office_homepage');

    }


    //DELETE USER FROM DB
    public function deleteAction(Request $request){

        $session = $request->getSession();
        $id = $request->attributes->get('id');
        $em = $this->getDoctrine()->getEntityManager();
        $adminentities = $em->getRepository('parcOfficeBundle:Administrator')->find($id);
        $em->remove($adminentities);
        $em->flush();

        $session->getFlashBag()->add('success','The account has been deleted');

        return $this->redirectToRoute('parcoffice_admin', array('id' => $session->get('id_admin')));

    }

}
