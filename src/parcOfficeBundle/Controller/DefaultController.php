<?php

namespace parcOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('parcOfficeBundle:Default:index.html.twig');
    }
}
