<?php
// src/AppBundle/Command/CreateUserCommand.php
namespace parcOfficeBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class CreateUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:create-users')

            // the short description shown while running "php bin/console list"
            ->setDescription('Creates new users.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows you to create users...")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        function ping($host, $times = 1)
        {
            exec("/bin/ping -c 1 -W 1 $host", $out, $status);
            return $status === 0 && false !== strpos(join('', $out), '0%');
        }
        $file = fopen("etatMachine.txt", "w");
        if (ping('10.75.2.191'))
        {
            fwrite($file, "allumee");
            fclose($file);
        }
        else
        {
            fwrite($file, "eteinte");
            fclose($file);
        }
    }
}
