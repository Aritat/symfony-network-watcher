<?php

namespace parcOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Log
 *
 * @ORM\Table(name="log")
 * @ORM\Entity(repositoryClass="parcOfficeBundle\Repository\LogRepository")
 */
class Log
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content_log", type="string")
     */
    private $contentLog;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var int
     *
     * @ORM\Column(name="id_computer", type="integer")
     */
    private $id_computer;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contentLog
     *
     * @param string $contentLog
     *
     * @return Log
     */
    public function setContentLog($contentLog)
    {
        $this->contentLog = $contentLog;

        return $this;
    }

    /**
     * Get contentLog
     *
     * @return string
     */
    public function getContentLog()
    {
        return $this->contentLog;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Log
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set idComputer
     *
     * @param integer $idComputer
     *
     * @return Log
     */
    public function setIdComputer($idComputer)
    {
        $this->id_computer = $idComputer;

        return $this;
    }

    /**
     * Get idComputer
     *
     * @return int
     */
    public function getIdComputer()
    {
        return $this->id_computer;
    }
}

